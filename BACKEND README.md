
# BACKEND

Desenvolvido em Java (SpringBoot)

## Requisitos

Java 8 / Gradle 2.13

## Instalação

Na pasta do projeto digite:

```
gradle bootRun
```

## Comentários

Gostaria de ter implementado no projeto "filas" e explorado mais a orientação a objetos, mas devido ao meu tempo dei preferencia a entregar o projeto e explicar melhor em uma possível entrevista
