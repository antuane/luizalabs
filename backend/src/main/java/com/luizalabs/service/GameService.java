package com.luizalabs.service;

import com.luizalabs.model.Game;
import com.luizalabs.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by antuane on 6/11/16.
 */
@Service
public class GameService {

    @Autowired
    GameRepository gameRepository;

    @Transactional
    public List<Game> findAll() {
        return gameRepository.findAll();
    }

    @Transactional
    public Game findById(Integer id) {
        return gameRepository.findOne(id);
    }

    @Transactional
    public Game save(Game game) {
        return gameRepository.save(game);
    }


}
