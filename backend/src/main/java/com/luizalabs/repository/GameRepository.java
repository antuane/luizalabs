package com.luizalabs.repository;

import com.luizalabs.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by antuane on 6/11/16.
 */
public interface GameRepository extends JpaRepository<Game, Integer> {
}
