'use strict';

describe('Controller: MainCtrl', function () {

  beforeEach(module('luizalabsApp'));

  var MainCtrl,
    scope;

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });

    scope.listType = [
      { id: 'Run', name: 'Run' },
      { id: 'Bike', name: 'Bike' },
      { id: 'Swimming', name: 'Swimming' }
    ];

    scope.listTimeSpent = [
      {'itemTime':'01:00','itemType':'Run','itemDate':'2016-06-14T03:00:00.000Z'},
      {'itemTime':'03:00','itemType':'Run','itemDate':'2016-06-23T03:00:00.000Z'},
      {'itemTime':'02:30','itemType':'Bike','itemDate':'2016-06-08T03:00:00.000Z'}
    ];

    scope.sumTimeTotal = "";

  }));

  it('deve conter uma lista com 3 elementos', function () {
    expect(scope.listType.length).toBe(3);
  });

  it('deve alterar o statos da variavel formSubmitted ao chamar a funcao addItem', function () {
    expect(scope.formSubmitted).toBe(false);
    scope.addItem({});
    expect(scope.formSubmitted).toBe(true);
  });

  it('deve alterar o statos da variavel formSubmitted ao chamar a funcao addItem', function () {
    expect(scope.formSubmitted).toBe(false);
    scope.addItem({});
    expect(scope.formSubmitted).toBe(true);
  });

  it('deve remover um item da lista listTimeSpent ao chamar a funcao removeItem', function () {
    expect(scope.listTimeSpent.length).toBe(3);
    scope.removeItem(0);
    expect(scope.listTimeSpent.length).toBe(2);

  });

  it('deve remover um item da lista', function () {
    expect(scope.listTimeSpent.length).toBe(3);
    scope.removeItem(0);
    expect(scope.listTimeSpent.length).toBe(2);
  });

  it('deve somar os valores da lista um item da lista listTimeSpent ao chamar a funcao removeItem', function () {
    expect(scope.sumTimeTotal).toBe("");
    scope.sumTime();
    expect(scope.sumTimeTotal).toBe("6:30");
  });



});
