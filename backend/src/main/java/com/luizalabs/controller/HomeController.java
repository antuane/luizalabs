package com.luizalabs.controller;

import com.luizalabs.service.LogProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by antuane on 6/11/16.
 */
@Controller
public class HomeController {

    @Autowired
    private LogProcessorService logProcessorService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index (Model model) {
        return "index";
    }

    @RequestMapping(value = "/logprocessor", method = RequestMethod.GET)
    public String logprocessor (Model model) {
        logProcessorService.processFile();
        model.addAttribute("message", "Log processado com sucesso!");
        return "index";

    }

}