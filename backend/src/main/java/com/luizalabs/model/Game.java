package com.luizalabs.model;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.*;

/**
 * Created by antuane on 6/11/16.
 */
@Entity
@Table(name = "game")
public class Game {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer totalKills;
    private List<Player> players;

//    private String sv_floodProtect;
//    private String sv_maxPing;
//    private String sv_minPing;
//    private String sv_maxRate;
//    private String sv_minRate;
//    private String sv_hostname;
//    private String g_gametype;
//    private String sv_privateClients;
//    private String sv_maxclients;
//    private String sv_allowDownload;
//    private String dmflags;
//    private String fraglimit;
//    private String timelimit;
//    private String g_maxGameClients;
//    private String capturelimit;
//    private String version;
//    private String protocol;
//    private String mapname;
//    private String gamename;
//    private String g_needpass;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "total_kills")
    public Integer getTotalKills() {
        return totalKills;
    }

    public void setTotalKills(Integer totalKills) {
        this.totalKills = totalKills;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "game", cascade = CascadeType.ALL)
    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Game() {
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", totalKills=" + totalKills +
                ", players=" + players +
                '}';
    }
}
