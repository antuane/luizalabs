
# FRONTEND

Desenvolvido em Angular

## Requisitos

NODE NPM, RUBY

```
gem install compass
npm install -g grunt-cli bower yo generator-karma generator-angular
```

## Instalação

```
gem install compass
npm install -g grunt-cli bower
npm Install
```

Para rodar, na pasta do projeto digite:

```
grunt server
```

## Testes

```
npm test
```

## Comentários

Gostaria de ter usado o Materialize Css (http://materializecss.com/), porém quando vi o prazo se esgotando não vi outra alternativa se não o Bootstrap, peço desculpas e espero que compreendam!
