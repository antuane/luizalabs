package com.luizalabs.service;

import com.luizalabs.helper.LogHelper;
import com.luizalabs.repository.GameRepository;
import com.luizalabs.model.Game;
import com.luizalabs.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by antuane on 6/11/16.
 */
@Service
public class LogProcessorService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private File file = null;

    @Autowired
    private GameRepository gameDAO;

    @PostConstruct
    public void init() {
        try {
            logger.info("LogProcessorService: Trying to load Doom Log file...");

            ClassLoader classLoader = getClass().getClassLoader();
            file = new File(classLoader.getResource("data/games.log").getFile());

        } catch (Exception e) {
            logger.error("LogProcessorService: Error to load Doom Log file. ", e);
        }
    }


    public void processFile(){
        try (Scanner scanner = new Scanner(file)) {

            Game game = new Game();
            List<Player> listPlayers = new ArrayList<>();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String header = LogHelper.getHeaderKey(line);

                switch (header) {
                    case "InitGame":
                        game = new Game();
                        gameDAO.save(game);
                        game.setTotalKills(0);
                        listPlayers = new ArrayList<>();
                        break;
                    case "ClientConnect":
                        Player playerNew = new Player();
                        playerNew.setGame(game);
                        playerNew.setClientId(LogHelper.getValue(line));
                        playerNew.setKills(0);
                        playerNew.setDeads(0);
                        listPlayers.add(playerNew);
                        break;
                    case "ClientUserinfoChanged":
                        String valueUserInfo = LogHelper.getValue(line);
                        String idPl = valueUserInfo.split(" ")[0];
                        Player playerChanged = listPlayers.stream().filter(x -> x.getClientId().equals(idPl)).findFirst().orElse(null);
                        String name = LogHelper.getValueByKey(valueUserInfo, "n");
                        playerChanged.setName(name);
                        break;
                    case "Kill":
                        String[] kills = LogHelper.getValue(line).split(" ");
                        Player killer =  listPlayers.stream().filter(x -> x.getClientId().equals(kills[0])).findFirst().orElse(null);
                        if(killer != null){
                            killer.setKills(killer.getKills() + 1);
                        }
                        Player killed =  listPlayers.stream().filter(x -> x.getClientId().equals(kills[1])).findFirst().orElse(null);
                        if(killed != null){
                            killed.setDeads(killed.getKills() + 1);
                        }
                        game.setTotalKills(game.getTotalKills() + 1);
                        break;
                    case "ShutdownGame":
                        game.setPlayers(listPlayers);
                        gameDAO.save(game);
                        break;
                    default:
                        logger.info("LogProcessorService: No match line.");
                }

            }

            logger.info("LogProcessorService: Success to read file.");

            scanner.close();

        } catch (IOException e) {
            logger.error("LogProcessorService: Error to read line file. ", e);
        }

    }


}
