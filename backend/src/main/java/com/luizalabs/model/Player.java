package com.luizalabs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.*;

/**
 * Created by antuane on 6/11/16.
 */
@Entity
@Table(name = "player")
public class Player {

    private static final long serialVersionUID = 2L;
    private Integer id;
    private String clientId;
    private String name;
    private int kills;
    private int deads;
    private Game game;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "client_id")
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "kills")
    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    @Column(name = "deads")
    public int getDeads() {
        return deads;
    }

    public void setDeads(int deads) {
        this.deads = deads;
    }

    @ManyToOne
    @JoinColumn(name = "game_id")
    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player() {
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", clientId='" + clientId + '\'' +
                ", name='" + name + '\'' +
                ", kills=" + kills +
                ", deads=" + deads +
                '}';
    }
}
