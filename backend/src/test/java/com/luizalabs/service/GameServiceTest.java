package com.luizalabs.service;

import com.luizalabs.BackendApplicationTests;
import com.luizalabs.model.Game;
import com.luizalabs.repository.GameRepository;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by antuane on 6/12/16.
 */
public class GameServiceTest extends BackendApplicationTests {

    @Autowired
    private GameService gameService;

    @Autowired
    private GameRepository gameRepository;

    @Before
    public void setUp() {
        gameRepository.deleteAll();
    }

    @Test
    public void testRepositoryIsEmpty() {
        assertTrue(gameService.findAll().isEmpty());
    }

    @Test
    public void testSaveAnGame() throws Exception {
        try{
            Game game01 = new Game();
            game01.setId(1);
            game01.setTotalKills(50);
            gameService.save(game01);
            assertFalse(gameService.findAll().isEmpty());
        }catch (Exception e) {
            fail("not expected result");
        }
    }


}
