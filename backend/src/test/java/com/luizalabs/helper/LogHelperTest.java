package com.luizalabs.helper;

import com.luizalabs.model.Game;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by antuane on 6/13/16.
 */
public class LogHelperTest {

    final String INIT_GAME_LINE 	= "  0:00 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\10000\\sv_minRate\\0\\sv_hostname\\Code Miner Server\\g_gametype\\0\\sv_privateClients\\2\\sv_maxclients\\16\\sv_allowDownload\\0\\dmflags\\0\\fraglimit\\20\\timelimit\\15\\g_maxGameClients\\0\\capturelimit\\8\\version\\ioq3 1.36 linux-x86_64 Apr 12 2009\\protocol\\68\\mapname\\q3dm17\\gamename\\baseq3\\g_needpass\\0";
    final String CLIENT_CONNETC_LINE= "  1:47 ClientConnect: 3";
    final String CLIENT_CHANGED_LINE= " 20:34 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\0\\model\\xian/default\\hmodel\\xian/default\\g_redteam\\\\g_blueteam\\\\c1\\4\\c2\\5\\hc\\100\\w\\0\\l\\0\\tt\\0\\tl\\0\n";
    final String KILL_LINE 			= " 3:13 Kill: 3 2 6: Isgalamido killed Dono da Bola by MOD_ROCKET";
    final String WORLD_KILL_LINE	= " 21:07 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT";
    final String SHUTDOWN_LINE   	= " 54:21 ShutdownGame:";


    @Test
    public void headerLineTest() {
        assertEquals(LogHelper.getHeaderKey(INIT_GAME_LINE), "InitGame");
        assertEquals(LogHelper.getHeaderKey(CLIENT_CONNETC_LINE), "ClientConnect");
        assertEquals(LogHelper.getHeaderKey(CLIENT_CHANGED_LINE), "ClientUserinfoChanged");
        assertEquals(LogHelper.getHeaderKey(KILL_LINE), "Kill");
        assertEquals(LogHelper.getHeaderKey(WORLD_KILL_LINE), "Kill");
        assertEquals(LogHelper.getHeaderKey(SHUTDOWN_LINE), "ShutdownGame");
    }

    @Test
    public void timeLineTest() {
        assertEquals(LogHelper.getTime(INIT_GAME_LINE), "0:00");
        assertEquals(LogHelper.getTime(CLIENT_CONNETC_LINE), "1:47");
        assertEquals(LogHelper.getTime(CLIENT_CHANGED_LINE), "20:34");
        assertEquals(LogHelper.getTime(KILL_LINE), "3:13");
        assertEquals(LogHelper.getTime(WORLD_KILL_LINE), "21:07");
        assertEquals(LogHelper.getTime(SHUTDOWN_LINE), "54:21");
    }

    @Test
    public void valueLineTest() {
        assertEquals(LogHelper.getValue(INIT_GAME_LINE), "\\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\10000\\sv_minRate\\0\\sv_hostname\\Code Miner Server\\g_gametype\\0\\sv_privateClients\\2\\sv_maxclients\\16\\sv_allowDownload\\0\\dmflags\\0\\fraglimit\\20\\timelimit\\15\\g_maxGameClients\\0\\capturelimit\\8\\version\\ioq3 1.36 linux-x86_64 Apr 12 2009\\protocol\\68\\mapname\\q3dm17\\gamename\\baseq3\\g_needpass\\0");
    }

    @Test
    public void valueByKeyLineTest() {
        assertEquals(LogHelper.getValueByKey(INIT_GAME_LINE, "sv_floodProtect"), "1");
        assertEquals(LogHelper.getValueByKey(INIT_GAME_LINE, "sv_maxPing"), "0");
        assertEquals(LogHelper.getValueByKey(INIT_GAME_LINE, "sv_minPing"), "0");
        assertEquals(LogHelper.getValueByKey(INIT_GAME_LINE, "sv_maxRate"), "10000");
        assertEquals(LogHelper.getValueByKey(INIT_GAME_LINE, "sv_minRate"), "0");
        assertEquals(LogHelper.getValueByKey(INIT_GAME_LINE, "sv_hostname"), "Code Miner Server");
        assertEquals(LogHelper.getValueByKey(CLIENT_CHANGED_LINE, "n"), "Isgalamido");
    }
}
