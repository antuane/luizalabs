package com.luizalabs.controller;

import com.luizalabs.model.Game;
import com.luizalabs.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by antuane on 6/11/16.
 */
@RestController
public class GameController {

    @Autowired
    GameService gameService;

    @RequestMapping(value = "/games", method = RequestMethod.GET)
    public List<Game> listGames() {
        return gameService.findAll();
    }

    @RequestMapping(value = "/game/{id}", method = RequestMethod.GET)
    public Game game(@PathVariable("id") Integer id) {
        return gameService.findById(id);
    }

}
