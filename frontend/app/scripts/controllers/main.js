'use strict';

/**
 * @ngdoc function
 * @name luizalabsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the luizalabsApp
 */
angular.module('luizalabsApp')
  .controller('MainCtrl', function ($scope) {

    $scope.sortType     = 'itemDate';
    $scope.sortReverse  = true;
    $scope.searchQuery   = '';

    $scope.listType = [
      { id: 'Run', name: 'Run' },
      { id: 'Bike', name: 'Bike' },
      { id: 'Swimming', name: 'Swimming' }
    ];

    $scope.sumTimeTotal = '';

    $scope.formSubmitted = false;

    $scope.listTimeSpent = [];

    $scope.clearForm = function(){
      $scope.itemTime = '';
      $scope.itemType = '';
      $scope.itemDate = '';
      $scope.formSubmitted = false;
    };

    var secondTransform = function(item){
      console.log(item);
      var total = 0;
      var times = item.split(':');
      total += times[0] * 60 * 60;
      total += times[1] * 60;
      return total;
    };

    $scope.sumTime = function(){
      var delta = 0;
      for (var i = 0; i < $scope.listTimeSpent.length; i++) {
        delta += secondTransform($scope.listTimeSpent[i].itemTime);
      }
      $scope.sumTimeTotal = '';
      var days = Math.floor(delta / 86400);
      delta -= days * 86400;
      $scope.sumTimeTotal += (days > 0 ? days + ' dias ' : '');
      var hours = Math.floor(delta / 3600) % 24;
      delta -= hours * 3600;
      $scope.sumTimeTotal += hours > 0 ? hours + ':' : '00:';
      var minutes = Math.floor(delta / 60) % 60;
      delta -= minutes * 60;
      $scope.sumTimeTotal += minutes > 0 ? minutes + '' : '00';
    };

    $scope.sumTime();

    $scope.addItem = function(form){
      $scope.formSubmitted = true;
      if(form.$valid){
        $scope.formSubmitted = false;
        $scope.listTimeSpent.push({
          itemTime: $scope.itemTime,
          itemType: $scope.itemType,
          itemDate: $scope.itemDate
        });
        $scope.clearForm();
        $scope.sumTime();
      }
    };

    $scope.removeItem = function(index){
      if (index > -1) {
          $scope.listTimeSpent.splice(index, 1);
          $scope.sumTime();
      }
    };


  });
