package com.luizalabs.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by antuane on 6/13/16.
 */
public class LogHelper {

    public static  String getHeaderKey(String line){
        String p = new StringBuilder("[0-9:]+[\\s]*([^:]+)").toString();
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()){
            return matcher.group(1);
        }
        return "";
    }

    public static  String getValue(String line){
        String p = new StringBuilder("[0-9:]+[\\s]*[^:]+\\:\\s([^\\n]+)").toString();
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()){
            return matcher.group(1);
        }
        return "";
    }

    public static  String getTime(String line){
        String p = new StringBuilder("([0-9:]+)[\\s]*").toString();
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()){
            return matcher.group(1);
        }
        return "";
    }

    public static  String getValueByKey(String line, String key){
        String p = new StringBuilder(key).append("\\\\([^\\\\]+)").toString();
        Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()){
            return matcher.group(1);
        }
        return "";
    }

}
